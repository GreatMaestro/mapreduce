﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace MasterService
{
    [ServiceContract]
    public interface IMasterService
    {
        [OperationContract]
        void RegisterWorker(Uri endpointAddress);

        [OperationContract]
        void Ping(Uri endpointAddress);
        [OperationContract]
        Dictionary<string, int[]> MapReduce(string[] source,
            Func<string[], Dictionary<string, int[]>> map,
            Func<Dictionary<string, int[]>, Dictionary<string, int>> reduce);
    }
}
