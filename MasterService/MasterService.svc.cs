﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using WorkerServiceLib;
using Timer = System.Timers.Timer;

namespace MasterService
{

    public class Master : IMasterService
    {
        private readonly ConcurrentBag<Worker> _workers = new ConcurrentBag<Worker>();

        public void RegisterWorker(Uri endpointAddress)
        {
            var binding = new BasicHttpBinding();
            var address = new EndpointAddress(endpointAddress);
            var workerService = new ChannelFactory<IWorkerService>(binding, address).CreateChannel();
            var worker = new Worker(workerService, endpointAddress);

            var pingDelay = new Timer();
            pingDelay.Elapsed += (sender, e) => DisableWorker(worker);
            pingDelay.Interval = 5000;
            pingDelay.Start();

            _workers.Add(worker);
        }

        public void Ping(Uri endpointAddress)
        {
            foreach (var worker in _workers)
            {
                if (worker.EndpointAddress == endpointAddress)
                {
                    worker.IsAvailable = true;
                    break;
                }
            }
        }

        private void DisableWorker(Worker worker)
        {
            if (worker.IsAvailable) worker.IsAvailable = false;
            else
            {
                _workers.TryTake(out worker);
                if (worker.CurrMapWork != null)
                {
                    while (true)
                    {
                        foreach (var w in _workers)
                        {
                            if (!w.IsAvailable || w.IsWorking) continue;
                            w.Map(worker.CurrMapWork);
                            return;
                        }
                        Thread.Sleep(1000);
                    }
                }
                if (worker.CurrReduceWork != null)
                {
                    while (true)
                    {
                        foreach (var w in _workers)
                        {
                            if (!w.IsAvailable || w.IsWorking) continue;
                            w.Reduce(worker.CurrReduceWork);
                            return;
                        }
                        Thread.Sleep(1000);
                    }
                }
            }
        }

        private Dictionary<Worker, string[]> PrepareForMap(IReadOnlyCollection<string> txtFiles)
        {
            int workersCount = _workers.Count;
            int count = txtFiles.Count;
            int[] counts = new int[workersCount];
            for (int i = 0; i < workersCount; i++)
            {
                counts[i] = count / workersCount;
                if (count % workersCount != 0)
                {
                    counts[i]++;
                    count--;
                }
            }

            Worker[] workers = _workers.ToArray();
            var workersToFiles = new Dictionary<Worker, string[]>();

            int skipCount = 0;
            for (int i = 0; i < workersCount; i++)
            {
                workersToFiles.Add(workers[i], txtFiles.Skip(skipCount).Take(counts[i]).ToArray());
                skipCount += counts[i];
            }

            return workersToFiles;
        }

        private Dictionary<Worker, Dictionary<string, int[]>> PrepareForReduce(Dictionary<string, List<int>> mapped)
        {
            int workersCount = _workers.Count;
            int count = mapped.Count;
            int[] counts = new int[workersCount];
            for (int i = 0; i < workersCount; i++)
            {
                counts[i] = count / workersCount;
                if (count % workersCount != 0)
                {
                    counts[i]++;
                    count--;
                }
            }

            Worker[] workers = _workers.ToArray();
            var workersToMapped = new Dictionary<Worker, Dictionary<string, int[]>>();

            int skipCount = 0;
            for (int i = 0; i < workersCount; i++)
            {
                workersToMapped.Add(workers[i], mapped.Skip(skipCount).Take(counts[i]).ToDictionary(pair => pair.Key, pair => pair.Value.ToArray()));
                skipCount += counts[i];
            }

            return workersToMapped;
        }

        public Dictionary<string, int[]> MapReduce(string[] source,
            Func<string[], Dictionary<string, int[]>> map,
            Func<Dictionary<string, int[]>, Dictionary<string, int>> reduce)
        {
            var txtFiles = new List<string>();
            foreach (var sourceDirectory in source)
            {
                txtFiles.AddRange(Directory.EnumerateFiles(sourceDirectory, "*.txt"));
            }

            #region Map

            var workersToFiles = PrepareForMap(txtFiles);
            var mapResults = new ConcurrentBag<KeyValuePair<string, int[]>>();
            Parallel.ForEach(workersToFiles, pair =>
            {
                pair.Key
                .Map(map, pair.Value)
                .AsParallel().ForAll(valuePair => mapResults.Add(valuePair));
            });

            #endregion

            var groupped = new Dictionary<string, List<int>>();
            foreach (var mapResult in mapResults)
            {
                var key = mapResult.Key;
                var value = mapResult.Value;
                if (groupped.ContainsKey(key)) groupped[key].AddRange(mapResult.Value);
                else groupped.Add(key, value.ToList());
            }

            #region Reduce

            var workersToMapped = PrepareForReduce(groupped);
            var reduceResults = new ConcurrentBag<KeyValuePair<string, int>>();
            Parallel.ForEach(workersToMapped, pair =>
            {
                pair.Key
                    .Reduce(reduce, pair.Value)
                    .AsParallel().ForAll(valuePair => reduceResults.Add(valuePair));
            });

            #endregion

            return reduceResults.ToDictionary(pair => pair.Key, pair => new[]{ pair.Value });
        }

        public class Worker
        {
            public IWorkerService WorkerService { get; }
            public Uri EndpointAddress { get; }
            public bool IsAvailable { get; set; }
            public bool IsWorking { get; set; }
            public MapWork CurrMapWork { get; set; }
            public ReduceWork CurrReduceWork { get; set; }

            public Worker(IWorkerService workerService, Uri endpointAddress)
            {
                WorkerService = workerService;
                EndpointAddress = endpointAddress;
                IsAvailable = true;
                IsWorking = false;
            }

            public Dictionary<string, int[]> Map(Func<string[], Dictionary<string, int[]>> map, string[] sourceFiles)
            {
                IsWorking = true;
                CurrMapWork = new MapWork(map, sourceFiles);
                var mapResult = WorkerService.Map(map, sourceFiles);
                CurrMapWork = null;
                IsWorking = false;
                return mapResult;
            }

            public Dictionary<string, int[]> Map(MapWork mapWork)
            {
                CurrMapWork = mapWork;
                return Map(mapWork.Map, mapWork.SourceFiles);
            }

            public Dictionary<string, int> Reduce(Func<Dictionary<string, int[]>, Dictionary<string, int>> reduce,
                Dictionary<string, int[]> mapped)
            {
                IsWorking = true;
                CurrReduceWork = new ReduceWork(reduce, mapped);
                var reduceResult = WorkerService.Reduce(reduce, mapped);
                CurrReduceWork = null;
                IsWorking = false;
                return reduceResult;
            }

            public Dictionary<string, int> Reduce(ReduceWork reduceWork)
            {
                CurrReduceWork = reduceWork;
                return Reduce(reduceWork.Reduce, reduceWork.Mapped);
            }

            public class MapWork
            {
                public Func<string[], Dictionary<string, int[]>> Map { get; }
                public string[] SourceFiles { get; }

                public MapWork(Func<string[], Dictionary<string, int[]>> map, string[] sourceFiles)
                {
                    Map = map;
                    SourceFiles = sourceFiles;
                }
            }

            public class ReduceWork
            {
                public Func<Dictionary<string, int[]>, Dictionary<string, int>> Reduce { get; }
                public Dictionary<string, int[]> Mapped { get; }
                public ReduceWork(Func<Dictionary<string, int[]>, Dictionary<string, int>> reduce, Dictionary<string, int[]> mapped)
                {
                    Reduce = reduce;
                    Mapped = mapped;
                }
            }
        }

        
    }
}

