﻿using System;
using System.Collections.Generic;

namespace WorkerServiceLib
{
    public class WorkerService : IWorkerService
    {
        public Dictionary<string, int[]> Map(Func<string[], Dictionary<string, int[]>> map, string[] sourceFiles)
        {
            return map(sourceFiles);
        }

        public Dictionary<string, int> Reduce(Func<Dictionary<string, int[]>, Dictionary<string, int>> reduce, Dictionary<string, int[]> mapped)
        {
            return reduce(mapped);
        }
    }
}
