﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace WorkerServiceLib
{
    [ServiceContract]
    public interface IWorkerService
    {
        [OperationContract]
        Dictionary<string, int[]> Map(Func<string[], Dictionary<string, int[]>> map, string[] sourceFiles);
        [OperationContract]
        Dictionary<string, int> Reduce(Func<Dictionary<string, int[]>, Dictionary<string, int>> reduce, Dictionary<string, int[]> mapped);
    }
}
