﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Timers;
using WorkerServiceHost.MasterRef;
using WorkerServiceLib;

namespace WorkerServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            var baseAddress = new Uri("http://localhost:" + args[0] + "/MapReduce/Worker");
            var selfHost = new ServiceHost(typeof(WorkerService), baseAddress);
            try
            {
                selfHost.AddServiceEndpoint(typeof(IWorkerService), new WSHttpBinding(), "Worker");

                var smb = new ServiceMetadataBehavior { HttpGetEnabled = true };
                selfHost.Description.Behaviors.Add(smb);

                selfHost.Open();

                var client = new MasterServiceClient();
                client.RegisterWorker(baseAddress);

                var pingDelay = new Timer();
                pingDelay.Elapsed += (sender, e) => client.Ping();
                pingDelay.Interval = 1000;
                pingDelay.Start();

                Console.WriteLine("The worker is launched on port " + args[0]);
                Console.WriteLine("Press <ENTER> to terminate service");
                Console.ReadLine();

                selfHost.Close();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("An exception occurred: {0}", ce.Message);
                selfHost.Abort();
                Console.ReadKey();
            }
        }
    }


}
