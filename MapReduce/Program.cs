﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using MapReduceSample.MasterRef;

namespace MapReduceSample
{
    internal class Program
    {
        /// <param name="args">Список директорий для обработки</param>
        private static void Main(string[] args)
        {
            foreach (var line in args)
            {
                Console.WriteLine(line);
            }
            Console.WriteLine();
            var client = new MasterServiceClient();
            var results = client.MapReduce(args, Map, Reduce);
            var sortedResults = results.OrderByDescending(s => s.Value[0]);
            // Отображение результата
            foreach (var result in sortedResults)
            {
                Console.WriteLine("{0} = {1}", result.Key, result.Value[0]);
            }
            Console.ReadKey();
        }

        private static Dictionary<string, int[]> Map(string[] sourceDirectories)
        {
            var txtFiles = new List<string>();
            foreach (var sourceDirectory in sourceDirectories)
            {
                txtFiles.AddRange(Directory.EnumerateFiles(sourceDirectory, "*.txt"));
            }

            var words = new List<string>();
            foreach (string currentFile in txtFiles)
            {
                var text = File.ReadAllText(currentFile);
                words.AddRange(new Regex("[^a-zA-Zа-яА-Я-\\s]")
                    .Replace(text, "")
                    .ToLower()
                    .Split()
                    .Where(s => s != ""));
            }
            var dict = new Dictionary<string, List<int>>();
            foreach (var word in words)
            {
                if (!dict.ContainsKey(word))
                    dict.Add(word, new List<int>{1});
                else
                    dict[word].Add(1);
            }
            return dict.AsParallel().ToDictionary(k => k.Key, v => v.Value.ToArray());
        }

        private static Dictionary<string, int> Reduce(Dictionary<string, int[]> reduceItems)
        {
            return reduceItems.AsParallel().ToDictionary(item => item.Key, item => reduceItems[item.Key].Sum());
        }
    }
}
