﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using MasterService;

namespace MasterServiceHost
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var baseAddress = new Uri("http://localhost:8000/MapReduce/Master");
            var selfHost = new ServiceHost(typeof(Master), baseAddress);
            try
            {
                selfHost.AddServiceEndpoint(typeof(IMasterService), new WSHttpBinding(), "Master");

                var smb = new ServiceMetadataBehavior { HttpGetEnabled = true };
                selfHost.Description.Behaviors.Add(smb);

                selfHost.Open();
                Console.WriteLine("The master is launched.");
                Console.WriteLine("Press <ENTER> to terminate service");
                Console.ReadLine();

                selfHost.Close();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("An exception occurred: {0}", ce.Message);
                selfHost.Abort();
                Console.ReadKey();
            }
        }
    }
}
